FROM golang:latest

# RUN add-apt-repository
ADD . /app
ENTRYPOINT /app
WORKDIR /app

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -o main .

#FROM alpine:3.8
FROM scratch
COPY --from=0 /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=0 /app/main /app/
CMD ["/app/main"]
